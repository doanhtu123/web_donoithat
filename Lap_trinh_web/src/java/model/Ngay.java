/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class Ngay {
    String ma;
    String ten;
    String chiso ;

    public Ngay() {
    }

    public Ngay(String ma, String ten, String chiso) {
        this.ma = ma;
        this.ten = ten;
        this.chiso = chiso;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getChiso() {
        return chiso;
    }

    public void setChiso(String chiso) {
        this.chiso = chiso;
    }
     @Override
   public String toString() {
       return "Ngay[ma="+ma+",ten="+ten+", chiso="+chiso+"]";
    }
    
}
