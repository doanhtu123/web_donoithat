/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Ngay;
import model.Thang;

/**
 *
 * @author Dell
 */
public class BieudoNServlet extends HttpServlet {

 

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          String date = request.getParameter("date");
        ArrayList<Ngay> day = (new  BieudoNDAO()).bieudoNgay(date);
     String nam = date.substring(0,date.indexOf("-"));
     String thang = date.substring(date.indexOf("-")+1,date.length()-3);
     String ngay = date.substring(date.length()-2);
     String xemdate = ngay+" / "+ thang+" / "+nam;
//         String month1 ="2018-12";
        
        HttpSession session = request.getSession();
        session.setAttribute("xemdate", xemdate);
        session.setAttribute("date", day);
        response.sendRedirect("/Lap_trinh_web/BieudoNgay.jsp");
        
    }

  
}
