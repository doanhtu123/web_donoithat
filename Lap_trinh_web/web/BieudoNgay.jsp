


<%@page import="model.Ngay"%>
<%@page import="javafx.scene.control.Alert"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="model.Value"%>
<%@page import="java.util.Map"%>
<%@page import="controller.BieudoTDAO"%>
<%@page import="model.Thang"%>
<%@page import="model.Hoadon"%>
<%@page import="controller.BillDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thống kê theo tháng</title>
        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />
        <script src="${root}/js/jquery-1.11.1.min.js"></script>
    </head>
    <body>


        <%
              String check = (String) session.getAttribute("check");
        if(check==null){
            response.sendRedirect("loginAdmin.jsp");
        }
        
             String xemdate = (String) session.getAttribute("xemdate");
             if(xemdate==null){
                 xemdate ="";
             }
//                     ArrayList<Thang> mapTS = new ArrayList<Thang>();
            ArrayList<Ngay> mapTS = (ArrayList<Ngay>) session.getAttribute("date");
        %>		

        <jsp:include page ="/Admin/headerAdmin.jsp"></jsp:include>


            <div id="wrapper">
             <div id="leftBar">
	<ul>
		<li><a href="indexAdmin.jsp">Trang chủ</a></li>
		<li><a href="Admin/qlsanpham.jsp"> Sản Phẩm</a></li>
                <li><a href="Admin/qltaikhoan.jsp"> Tài Khoản</a></li>
                <li><a href="Admin/qlhoadon.jsp"> Hóa đơn</a></li>
		<li><a href="${root}/Admin/thongke.jsp">Thống kê</a></li>
	</ul>
	</div>
                <div id="rightContent">
                    <h3>Danh sách sản phẩm đã bán của : <%=xemdate%></h3>

                    <form action="BieudoNServlet" method="POST">
                        <input type="date" name="date">
                        <input type="submit" value="Xem">
                    </form>

                    <table class="data">
                        <tr class="data">
                            <th class="data" width="30px">STT</th>
                            <th class="data">Tên sản phẩm</th>
                            <th class="data">Số lượng bán ra</th>

                        </tr>
                    <%
                    if (mapTS != null) {
                            int count = 0;
                            for (Ngay ts : mapTS) {
                                count++;
                              
                    %>     
                    <tr class="data">
                        <td class="data" width="30px"><%=count%></td>
                        <td class="data"><%=ts.getTen()%></td>
                        <td class="data"><%=ts.getChiso()%></td>				
                    </tr> 
                    
                    <%}%>
                    <a style="float: right; margin-top: -20px" href="/Lap_trinh_web/Admin/chartDay.jsp">Xem biểu đồ</a>
                    

                    <%}%>
                  
                </table>
                    <a href="${root}/Admin/thongke.jsp" style="float: right">Quay lại</a>
            </div>
                    
            <div class="clear"></div>
              
            <jsp:include page ="/Admin/footerAdmin.jsp"></jsp:include>
        </div>


    </body>
</html>
