<%-- 
    Document   : indexAdmin
    Created on : Dec 28, 2018, 2:15:36 PM
    Author     : Dell
--%>

<%@page import="model.Hoadon"%>
<%@page import="controller.BillDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Biểu đồ hóa đơn</title>
        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />
<!--        <script src="${root}/js/jquery-1.11.1.min.js"></script>-->
         <script type="text/javascript" src="https://www.google.com/jsapi"></script>
         <script type="text/javascript">
            // Load the Visualization API and the piechart package.
            google.load('visualization', '1', {'packages': ['corechart']});

            // Set a callback to run when the Google Visualization API is loaded.
            google.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table,
            // instantiates the pie chart, passes in the data and
            // draws it.
            function drawChart() {

                // Create the data table.    
                var data = google.visualization.arrayToDataTable([
                    ['Country', 'Area(square km)'],
                    
                    <c:forEach var ="item" items="${month}">['${item.ten}',${item.chiso}],</c:forEach> 
                   
                ]);
                // Set chart options
                var options = {
                    'title': 'Biểu đồ tròn với tỉ lệ tương ứng',
                    is3D: true,
//                    pieSliceText: 'label',
                    tooltip: {showColorCode: true},
//                    sliceVisibilityThreshold: .2,
                    'width': 700,
                    'height': 400
                };

                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById('Chart'));
                chart.draw(data, options);
            }
        </script>
    </head>
    <body>
            <%
                String check = (String) session.getAttribute("check");
        if(check==null){
            response.sendRedirect("/Lap_trinh_web/loginAdmin.jsp");
        }
                String xem = (String) session.getAttribute("xem");
            %>
     
        <jsp:include page ="headerAdmin.jsp"></jsp:include>
         <div id="wrapper">
             <jsp:include page ="menuAdmin.jsp"></jsp:include>
           
            <div id="rightContent">
            <h3>Biểu đồ sản phẩm bán ra của tháng: <%=xem%></h3>
	
	
		<table class="data">
			<div id="Chart"></div>
                        <a href="${root}/BieudoThang.jsp" style="float: right; margin-right:10px">Quay lại</a>
		</table>
            <!--biến $ {root} xóa bỏ phần tử trc nó-->
	</div>
<div class="clear"></div>
            <jsp:include page ="footerAdmin.jsp"></jsp:include>
        </div>
        
        
    </body>
</html>
