<%-- 
    Document   : indexAdmin
    Created on : Dec 28, 2018, 2:15:36 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thống kê</title>
        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />
        <script src="${root}/js/jquery-1.11.1.min.js"></script>
    </head>
    <body>
        <% String check = (String) session.getAttribute("check");
        if(check==null){
            response.sendRedirect("/Lap_trinh_web/loginAdmin.jsp");
        }
        %>
        <jsp:include page ="headerAdmin.jsp"></jsp:include>
         <div id="wrapper">
            <jsp:include page ="menuAdmin.jsp"></jsp:include>
            <div id="rightContent">
            <h3>Danh mục cần thống kê</h3>
            <ul>
                <li><a href="${root}/BieudoNgay.jsp">Thống kê theo ngày</a> </li>
                <li><a href="${root}/BieudoThang.jsp">Thống kê theo tháng</a></li>
            <li><a href="${root}/ChartServelet">Thống kê toàn bộ sản phẩm đã bán</a> </li>
                   
            </ul>
	
	
	</div>
<div class="clear"></div>
            <jsp:include page ="footerAdmin.jsp"></jsp:include>
        </div>
        
        
    </body>
</html>
