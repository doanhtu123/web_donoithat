<%-- 
    Document   : indexAdmin
    Created on : Dec 28, 2018, 2:15:36 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý tài khoản</title>
        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />
        <script src="${root}/js/jquery-1.11.1.min.js"></script>
    </head>
    <body>
        <% String check = (String) session.getAttribute("check");
        if(check==null){
            response.sendRedirect("/Lap_trinh_web/loginAdmin.jsp");
        }
        %>
        <jsp:include page ="headerAdmin.jsp"></jsp:include>
         <div id="wrapper">
            <jsp:include page ="menuAdmin.jsp"></jsp:include>
            <div id="rightContent">
            <h3>Tài khoản</h3>
	
	
		<table class="data">
			<tr class="data">
				<th class="data" width="30px">No</th>
				<th class="data">Nama</th>
				<th class="data">Email</th>
				<th class="data">Telepon</th>
				<th class="data" width="75px">Pilihan</th>
			</tr>
			<tr class="data">
				<td class="data" width="30px">1</td>
				<td class="data">Data Anda</td>
				<td class="data">Data Anda</td>
				<td class="data">Data Anda</td>
				<td class="data" width="75px">
				<center>
				<a href="#"><img src="mos-css/img/oke.png"></a>&nbsp;&nbsp;&nbsp;
				<a href="#"><img src="mos-css/img/detail.png"></a>
				</center>
				</td>
			</tr>
		</table>
	</div>
<div class="clear"></div>
            <jsp:include page ="footerAdmin.jsp"></jsp:include>
        </div>
        
        
    </body>
</html>
